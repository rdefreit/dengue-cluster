---
title: "Clusters by municipality/year series, scaled, cuulative"
author: "Raphael Saldanha"
date: "2023-05-03"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Packages

```{r}
library(tidyverse)
library(lubridate)
library(arrow)
library(timetk)
library(dtwclust)
library(kableExtra)
library(tictoc)
```

## Data

Aggregated confirmed dengue cases per municipality and date, from 2000 to 2020-07.

```{r}
dengue <- read_parquet("../../dengue-data/parquet_aggregated/dengue_md.parquet")
```

### Data filtering and formating

-   Filter out dates after 2020-01-01, as this year is not complete
-   Aggregate cases by epidemiological week
-   Create separated time series by municipality and year
-   Accumulate time series by municipality and week
-   Filter out municipalities/year with less than 100 cases total
-   Scale cases
-   Format time series for clustering

```{r}
tdengue <- dengue %>%
  # Filter out dates after 2020-01-01, as this year is not complete
  filter(date <= as.Date("2020-01-01")) %>%
  # Pad days with zero cases
  group_by(mun) %>%
  pad_by_time(date, .by = "day", .pad_value = 0, .start_date = min(dengue$date), .end_date = max(dengue$date)) %>%
  ungroup() %>%
  # Aggregate by week
  mutate(
    week = str_pad(epiweek(date), pad = 0, width = 2),
    year = year(date)
  ) %>%
  group_by(mun, year, week) %>%
  summarise(freq = sum(freq, na.rm = TRUE)) %>%
  ungroup() %>%
  # Keep only municipalities/year series with more than 100 cases total
  group_by(mun, year) %>%
  mutate(total = sum(freq, na.rm = TRUE)) %>%
  ungroup() %>%
  filter(total >= 100) %>%
  select(-total) %>%
  # Modify freq
  group_by(mun, year) %>%
  arrange(week) %>%
  # Accumulate mun time series by year
  mutate(freq = cumsum(freq)) %>%
  # Scale cases by mun and year
  mutate(freq = scale(freq)) %>%
  ungroup() %>%
  arrange(mun, year, week) %>%
  # Isolate municipality and year
  mutate(mun = paste0(mun, "_", year)) %>%
  select(-year, week) %>%
  # Prepare time series of municipalities by year
  mutate(mun = paste0("m_", mun)) %>%
  arrange(mun) %>%
  pivot_wider(names_from = mun, values_from = freq) %>%
  select(-week) %>%
  t() %>%
  # Convert object
  tslist()

# Remove missing values created by leap years (week 53)
tdengue <- lapply(tdengue, na.omit)

# Save RDS
saveRDS(tdengue, file = "tdengue.rds", compress = "xz")
```

## K sequence

```{r}
k_seq <- 3:10
```

## DTW (basic)

Custom DTW version with less functionality, but faster.

```{r}
tic()
clust_dtwb <- tsclust(
  series = tdengue, 
  type = "partitional", 
  k = k_seq,
  distance = "dtw_basic",
  seed = 13
)
toc()
```

```{r}
saveRDS(clust_dtwb, file = "clust_dtwb.rds", compress = "xz")
```

### Cluster Validity Indices (CVI)

```{r}
names(clust_dtwb) <- paste0("k_", k_seq)
res_cvi <- sapply(clust_dtwb, cvi, type = "internal") %>% 
  t() %>%
  as_tibble(rownames = "k") %>%
  arrange(-Sil)

res_cvi %>%
  kbl() %>%
  kable_styling()
```

### Cluster with higher Silhouette statistic

```{r}
sel_clust <- clust_dtwb[[res_cvi[[1,1]]]]
plot(sel_clust, type = "series")
```

```{r}
plot(sel_clust, type = "centroids", lty = 1)
```

.

### Clusters sizes

```{r}
table(sel_clust@cluster)
```

## Soft DWT

A smoothed formulation of DTW <https://arxiv.org/abs/1703.01541>

```{r}
tic()
clust_sdtw <- tsclust(
  series = tdengue, 
  type = "partitional", 
  k = k_seq,
  distance = "sdtw",
  seed = 13
)
toc()
```

```{r}
saveRDS(clust_sdtw, file = "clust_sdtw.rds", compress = "xz")
```

### Cluster Validity Indices (CVI)

```{r}
names(clust_sdtw) <- paste0("k_", k_seq)
res_cvi <- sapply(clust_sdtw, cvi, type = "internal") %>% 
  t() %>%
  as_tibble(rownames = "k") %>%
  arrange(-Sil)

res_cvi %>%
  kbl() %>%
  kable_styling()
```

### Cluster with higher Silhouette statistic

```{r}
sel_clust <- clust_sdtw[[res_cvi[[1,1]]]]
plot(sel_clust, type = "series")
```

```{r}
plot(sel_clust, type = "centroids", lty = 1)
```

.

### Clusters sizes

```{r}
table(sel_clust@cluster)
```

## GAK

Distance based on (triangular) global alignment kernels.

> "Alternatives [that] are both faster and more efficient in classification tasks than other kernels based on the DTW formalism." <https://icml.cc/2011/papers/489_icmlpaper.pdf>

```{r}
tic()
clust_gak <- tsclust(
  series = tdengue, 
  type = "partitional", 
  k = k_seq,
  distance = "gak",
  seed = 13
)
toc()
```

```{r}
saveRDS(clust_gak, file = "clust_gak.rds", compress = "xz")
```

### Cluster Validity Indices (CVI)

```{r}
names(clust_gak) <- paste0("k_", k_seq)
res_cvi <- sapply(clust_gak, cvi, type = "internal") %>% 
  t() %>%
  as_tibble(rownames = "k") %>%
  arrange(-Sil)

res_cvi %>%
  kbl() %>%
  kable_styling()
```

### Cluster with higher Silhouette statistic

```{r}
sel_clust <- clust_gak[[res_cvi[[1,1]]]]
plot(sel_clust, type = "series")
```

```{r}
plot(sel_clust, type = "centroids", lty = 1)
```

.

### Clusters sizes

```{r}
table(sel_clust@cluster)
```

## SBD

Distance based on coefficient-normalized cross-correlation as proposed by Paparrizos and Gravano (2015) for the k-Shape clustering algorithm. <https://dl.acm.org/doi/10.1145/2723372.2737793>

```{r}
tic()
clust_sbd <- tsclust(
  series = tdengue, 
  type = "partitional", 
  k = k_seq,
  distance = "sbd",
  seed = 13
)
toc()
```

```{r}
saveRDS(clust_sbd, file = "clust_sbd.rds", compress = "xz")
```

### Cluster Validity Indices (CVI)

```{r}
names(clust_sbd) <- paste0("k_", k_seq)
res_cvi <- sapply(clust_sbd, cvi, type = "internal") %>% 
  t() %>%
  as_tibble(rownames = "k") %>%
  arrange(-Sil)

res_cvi %>%
  kbl() %>%
  kable_styling()
```

### Cluster with higher Silhouette statistic

```{r}
sel_clust <- clust_sbd[[res_cvi[[1,1]]]]
plot(sel_clust, type = "series")
```

```{r}
plot(sel_clust, type = "centroids", lty = 1)
```

.

### Clusters sizes

```{r}
table(sel_clust@cluster)
```
